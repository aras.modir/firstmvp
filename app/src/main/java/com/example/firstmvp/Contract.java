package com.example.firstmvp;

public interface Contract {
    interface View {
        void onAgeReceived(int age);
    }

    interface Presenter {
        void attatchView(View view);

        void recievedNameFamily(String name, String family);

        void onAgeReceived(int age);
    }

    interface Model {
        void attatchPresenter(Presenter presenter);

        void getAgeByNameFamily(String name, String family);
    }
}
