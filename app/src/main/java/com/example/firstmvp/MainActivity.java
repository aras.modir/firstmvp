package com.example.firstmvp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements Contract.View {
    private EditText name;
    private EditText family;
    private Button getData;

    Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindViews();

        presenter = new Presenter();
        presenter.attatchView(this);


    }

    private void bindViews() {
        name = findViewById(R.id.name);
        family = findViewById(R.id.family);
        getData = findViewById(R.id.getData);
        getData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.recievedNameFamily(name.getText().toString(),family.getText().toString());
            }
        });
    }

    @Override
    public void onAgeReceived(int age) {
        Toast.makeText(this, "my age is " + age, Toast.LENGTH_SHORT).show();
    }
}
