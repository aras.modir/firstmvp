package com.example.firstmvp;

public class Model implements Contract.Model {

    Contract.Presenter presenter;

    @Override
    public void attatchPresenter(Contract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getAgeByNameFamily(String name, String family) {
        if (name.equalsIgnoreCase("ali") && family.equalsIgnoreCase("hasani"))
            presenter.onAgeReceived(10);
        else
            presenter.onAgeReceived(30);
    }
}
