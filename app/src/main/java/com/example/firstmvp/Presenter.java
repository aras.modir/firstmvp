package com.example.firstmvp;

public class Presenter implements Contract.Presenter {

    Model model;
    Contract.View view;

    public Presenter() {
        model = new Model();
        model.attatchPresenter(this);
    }

    @Override
    public void attatchView(Contract.View view) {
        this.view = view;
    }

    @Override
    public void recievedNameFamily(String name, String family) {
        model.getAgeByNameFamily(name, family);
    }

    @Override
    public void onAgeReceived(int age) {
        view.onAgeReceived(age);
    }


}